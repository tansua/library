from __future__ import unicode_literals
from django.db import models
import re
# Create your models here.

# Create Novelist Model
class Novelist(models.Model):
    name = models.CharField(max_length=20)
    surname = models.CharField(max_length=20, unique=True)
    date_of_birth = models.DateField()

    def __str__(self):
        return ' '.join([self.name, self.surname])


    @classmethod
    def to_dict(cls, multi=True, **kwargs):
        novelists = cls.objects.all().filter(**kwargs)
        if novelists:
            return {
                novelist.id: {
                    'name': novelist.name,
                    'surname': novelist.surname,
                    'date_of_birth': novelist.date_of_birth.strftime('%d.%m.%Y')
                }
                for novelist in novelists
            } if len(novelists) > 1 or multi else {
                'name': novelists[0].name,
                'surname': novelists[0].surname,
                'date_of_birth': novelists[0].date_of_birth.strftime('%d.%m.%Y')
            }
        else:
            raise Novelist.DoesNotExist

# Create Book Model.
class Book(models.Model):
    title = models.CharField(max_length=35, unique=True)
    novelists = models.ManyToManyField(Novelist, 'novelists')
    lc_classification = models.CharField(max_length=10)

    def __str__(self):
        return ' '.join([self.title])


    @classmethod
    def to_dict(cls, multi=True, **kwargs):
        books = cls.objects.all().filter(**kwargs)
        if books:
            return {
                book.id: {
                    'title': book.title,
                    'lc_classification': book.lc_classification,
                    'authors': Novelist.to_dict(pk__in=[
                        novelist.id for novelist in book.novelists.all()
                        ])
                }
                for book in books
            } if len(books) > 1 or multi else {
                'title': books[0].title,
                'lc_classification': books[0].lc_classification,
                'novelists': Novelist.to_dict(pk__in=[
                    novelist.id for novelist in books[0].novelists.all()
                    ])
            }
        else:
            raise Book.DoesNotExist

