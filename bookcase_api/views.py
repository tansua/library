import book_detail_view
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import bookcase_view
import novelist_detail_view
import novelist_view
import books_view
from bookcase_http import Direction

# Create your views here.
# views list.

def index(r):
    return HttpResponse('Bookcase api run...')

@csrf_exempt
def bookcase_view(r):
    return Direction(r, bookcase_view).apply()

@csrf_exempt
def novelists_view(r):
    return Direction(r, novelist_view).apply()


@csrf_exempt
def novelist_detail_view(r, novelist_id):
    return Direction(r, novelist_detail_view).apply(
        novelist_id=novelist_id
    )


@csrf_exempt
def books_view_dimension(r):
    return Direction(r, books_view).apply()


@csrf_exempt
def book_detail_view(r, book_id):
    return Direction(r, book_detail_view).apply(book_id=book_id)
