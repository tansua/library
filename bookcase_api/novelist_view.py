from datetime import datetime
from django.http import Http404
from bookcase_api.models import Novelist
from bookcase_http import BookcaseRequest, BookcaseResponse


def get(r):
    try:
        return BookcaseResponse.custom_response(Novelist.to_dict())
    except Novelist.DoesNotExist:
        return BookcaseResponse.custom_response({})


def post(r):
    params = ['name', 'surname', 'date_of_birth']
    if BookcaseRequest(r).is_not_available(params):
        return BookcaseResponse.invalid_request()
    else:
        if not Novelist.fields_is_valid({k: r.POST[k] for k in params}):
            return BookcaseResponse.invalid_request_parameters()
        try:
            novelist = Novelist.objects.get(surname__iexact=r.POST['surname'])
            return BookcaseResponse.response(
                'error',
                'This author already exists'
            )
        except Novelist.DoesNotExist:
            new_novelist = Novelist(
                name=r.POST['name'],
                surname=r.POST['surname'],
                date_of_birth=datetime.strptime(
                    r.POST['date_of_birth'], '%d.%m.%Y'
                )
            )
            new_novelist.save()
            return BookcaseResponse.response(
                'success',
                'The author has been successfully added'
            )


def other(r):
    raise Http404()
