from django.http import HttpResponse, Http404, QueryDict
from bookcase_api.models import Novelist, Book
from bookcase_http import BookcaseResponse


def get(r, book_id):
    try:
        return BookcaseResponse.custom_response(Book.to_dict(
            multi=False, pk=book_id
        ))
    except Book.DoesNotExist:
        return BookcaseResponse.record_not_found()


def put(r, book_id):
    return patch(r, book_id)


def patch(r, book_id):
    query = QueryDict(r.body).copy()
    params = ['title', 'lc_classification', 'novelists']
    params_map = [query.get(p, False) for p in params]
    requests_is_not_available = True in [
        False in params_map and r.method == 'PUT',
        all(i is False for i in params_map) and r.method == 'PATCH'
    ]
    if requests_is_not_available:
        return BookcaseResponse.invalid_request()
    elif not Book.fields_is_valid(query):
        return BookcaseResponse.invalid_request_parameters()
    else:
        try:
            unique_book = False
            book = Book.objects.get(pk=book_id)
            if unique_book:
                return BookcaseResponse.response(
                    'error',
                    'This book already exists'
                )
            else:
                book_novelists = []
                for novelist_id in query['novelists'].split(','):
                    novelist = Novelist.objects.filter(pk=novelist_id)
                    if novelist:
                        book_novelists.append(Novelist.objects.get(pk=novelist_id))
                    else:
                        return BookcaseResponse.response(
                            'error',
                            'Novelist #%s not found' % novelist_id
                        )
                for field, value in query.items():
                    if field != 'novelists':
                        setattr(book, field, value)
                book.save()
                if book_novelists:
                    book.novelists.clear()
                    for novelist in book_novelists:
                        book.novelists.add(novelist)
                return BookcaseResponse.response(
                    'success',
                    'The book has been successfully updated'
                )
        except Book.DoesNotExist:
            return BookcaseResponse.record_not_found()


def delete(r, book_id):
    try:
        book = Book.objects.get(pk=book_id)
        book.delete()
        return BookcaseResponse.response(
            'success',
            'The book has been successfully deleted'
        )
    except Book.DoesNotExist:
        return BookcaseResponse.record_not_found()


def other(r, book_id):
    raise Http404()