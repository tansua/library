from __future__ import unicode_literals

from django.apps import AppConfig


class BookcaseApiConfig(AppConfig):
    name = 'bookcase_api'
