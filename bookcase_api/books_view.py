from django.http import Http404
from bookcase_api.models import Novelist, Book
from bookcase_http import BookcaseRequest, BookcaseResponse


def get(r):
    try:
        return BookcaseResponse.custom_response(Book.to_dict())
    except Book.DoesNotExist:
        return BookcaseResponse.custom_response({})

def post(r):
    params = ['title', 'lc_classification', 'novelists']
    if BookcaseRequest(r).is_not_available(params):
        return BookcaseResponse.invalid_request()
    elif not Book.fields_is_valid({k: r.POST[k] for k in params}):
        return BookcaseResponse.invalid_request_parameters()
    else:
        try:
            book = Book.objects.get(title__iexact=r.POST['title'])
            return BookcaseResponse.response(
                'error',
                'This book already exists'
            )
        except Book.DoesNotExist:
            new_book = Book(
                title=r.POST['title'],
                lc_classification=r.POST['lc_classification']
            )
            book_novelists = []
            for novelist_id in r.POST['novelists'].split(','):
                novelist = Novelist.objects.filter(pk=novelist_id)
                if novelist:
                    book_novelists.append(Novelist.objects.get(pk=novelist_id))
                else:
                    return BookcaseResponse.response(
                        'error',
                        'Novelist #%s not found' % novelist_id
                    )
            new_book.save()
            for novelist in book_novelists:
                new_book.novelists.add(novelist)
            return BookcaseResponse.response(
                'success',
                'The book has been successfully added'
            )


def other(r):
    raise Http404()
