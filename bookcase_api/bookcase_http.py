from django.http import HttpResponse, Http404
from bookcase_api.models import Novelist, Book
import json


class BookcaseResponse(object):
    @staticmethod
    def response(response_type, response_message):
        return HttpResponse(json.dumps({
            'response': response_type,
            'message': response_message
        }), content_type='application/json')

    @classmethod
    def invalid_request(cls):
        return cls.response('error', 'Request is not available')

    @classmethod
    def invalid_request_parameters(cls):
        return cls.response('error', 'Invalid request parameters')

    @classmethod
    def record_not_found(cls):
        return cls.response('error', 'Record(s) not found')

    @classmethod
    def invalid_csv_file(cls):
        return cls.response('error', 'Invalid .csv file')

    @staticmethod
    def custom_response(response):
        return HttpResponse(
            json.dumps(response),
            content_type='application/json'
        )


class BookcaseRequest(object):
    def __init__(self, request):
        self.r = request

    def is_not_available(self, params):
        return False in [self.r.POST.get(p, False) for p in params]

    @staticmethod
    def csv_file_is_not_available(csv_file):
        cols = [
            'title',
            'lc classification',
            'name',
            'surname',
            'date of birth'
        ]
        cols = cols + cols[3:] + cols[3:]
        csv_cols = csv_file.readline().lower().rstrip().split(';')
        if cols != csv_cols or sum(1 for _ in csv_file) <= 1:
            return True
        else:
            for line in csv_file:
                if '' in line.split(';')[:3] or '' in line.split(';')[3:6]:
                    return True
        return False

    @staticmethod
    def get_library_from_csv_file(csv_file):
        books = []
        for line_number, line in enumerate(csv_file):
            if line_number:
                line_list = line.rstrip().split(';')
                book = line_list[:3]
                novelist_scope = line_list[3:]
                novelists = []
                for i in xrange(0, 9, 3):
                    broken = '' in [novelist_scope[i+x] for x in xrange(0, 3)]
                    if not broken:
                        novelists.append({
                            'name': novelist_scope[i+0],
                            'surname': novelist_scope[i+1],
                            'date_of_birth': novelist_scope[i+2]
                        })
                books.append({
                    'title': book[0],
                    'lc_classification': book[1],
                    'novelists': novelists
                })
        return books

    @staticmethod
    def library_is_not_available(method, bookcase):
        surnames = []
        for book in bookcase:
            if not Book.fields_is_valid({
                'title': book['title'],
                'lc_classification': book['lc_classification'],
                'novelists': '0'
            }) or (method == 'PATCH'):
                return True
            else:
                same_line_surnames = []
                for novelist in book['novelists']:
                    if not Novelist.fields_is_valid({
                        'name': novelist['name'],
                        'surname': novelist['surname'],
                        'date_of_birth': novelist['date_of_birth']
                    }) or (method == 'PATCH' and
                            novelist['surname'] in same_line_surnames):
                        return True
                    else:
                        surnames.append(novelist['surname'])
                        same_line_surnames.append(novelist['surname'])
        if method == 'PATCH':
            for surname in surnames:
                try:
                    same_novelist = Novelist.objects.get(surname__iexact=surname)
                    return True
                except Novelist.DoesNotExist:
                    pass
        return False


class Direction(object):
    def __init__(self, request, module):
        self.r = request
        self.module = module
        self.method = self.r.method.lower() \
            if hasattr(self.module, self.r.method.lower()) \
            else 'other'

    def apply(self, **kwargs):
        return getattr(self.module, self.method)(r=self.r, **kwargs)
