from django.contrib import admin
from models import Novelist, Book

# Register your models here.

admin.site.register(Novelist)
admin.site.register(Book)
