from datetime import datetime
from django.http import Http404, HttpResponse
from bookcase_api.models import Novelist, Book
from bookcase_http import BookcaseRequest, BookcaseResponse
from django.core.files.base import ContentFile


def post(r):
    csv = None
    if r.method == 'POST':
        csv = r.FILES['file']
    elif r.method == 'PATCH':
        csv = ContentFile(r.body)
    else:
        return other(r)
    if BookcaseRequest.csv_file_is_not_available(csv):
        return BookcaseResponse.invalid_csv_file()
    else:
        bookcase = BookcaseRequest.get_library_from_csv_file(csv)
        if BookcaseRequest.library_is_not_available(r.method, bookcase):
            return BookcaseResponse.invalid_csv_file()
        else:
            if r.method == 'POST':
                Novelist.objects.all().delete()
                Book.objects.all().delete()
            for book in bookcase:
                book_novelists = []
                for novelist in book['novelists']:
                    try:
                        exists_novelist = Novelist.objects.get(
                            surname__iexact=novelist['surname']
                        )
                        book_novelists.append(exists_novelist)
                    except Novelist.DoesNotExist:
                        new_novelist = Novelist(
                            name=novelist['name'],
                            surname=novelist['surname'],
                            date_of_birth=datetime.strptime(
                                novelist['date_of_birth'], '%d.%m.%Y'
                            )
                        )
                        new_novelist.save()
                        book_novelists.append(new_novelist)
                new_book = Book(
                    title=book['title'],
                    lc_classification=book['lc_classification'],
                )
                new_book.save()
                for novelist in book_novelists:
                    new_book.novelists.add(novelist)
            return BookcaseResponse.response(
                'success',
                'The Bookcase has been successfully updated using the csv file'
            )


def patch(r):
    return post(r)


def other(r):
    raise Http404()
